import {
  CREATE_DISH_SUCCESS, DELETE_DISH_SUCCESS, EDIT_DISH_SUCCESS,
  FETCH_DISHES_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  dishes: []
};

const editDishSuccess = (state, action) => {
  const dishIndex = state.dishes.findIndex(dish => dish.id === action.id);

  const dishesCopy = [...state.dishes];
  dishesCopy[dishIndex] = {...dishesCopy[dishIndex], ...action.dish};

  return {...state, dishes: dishesCopy};
};

const deleteDishSuccess = (state, action) => {
  const dishIndex = state.dishes.findIndex(dish => dish.id === action.id);
  const dishesCopy = [...state.dishes];

  dishesCopy.splice(dishIndex, 1);

  return {...state, dishes: dishesCopy};
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_DISH_SUCCESS:
      return {...state, dishes: state.dishes.concat({...action.dish, id: action.id})};
    case FETCH_DISHES_SUCCESS:
      const dishes = Object.keys(action.dishes).map(key => {
        return {...action.dishes[key], id: key};
      });

      return {...state, dishes};
    case EDIT_DISH_SUCCESS:
      return editDishSuccess(state, action);
    case DELETE_DISH_SUCCESS:
      return deleteDishSuccess(state, action);
    default:
      return state;
  }
};

export default reducer;